Prognozowanie przyszłej ceny akcji spółki Kruk SA na podstawie modelu ARIMA w języku R
======================================================================================

__Treść analizy jest tylko i wyłącznie wyrazem osobistych poglądów jej autora i nie stanowi „rekomendacji” w rozumieniu przepisów Rozporządzenia Ministra Finansów z dnia 19 października 2005 r. w sprawie informacji stanowiących rekomendacje dotyczące instrumentów finansowych, lub ich emitentów (Dz. U. z 2005 r. Nr 206, poz. 1715). Analiza nie spełnia wymogów stawianych rekomendacjom w rozumieniu w/w ustawy, m.in. nie zawiera konkretnej wyceny instrumentu finansowego, nie opiera się na żadnej metodzie wyceny, a także nie określa ryzyka inwestycyjnego. Jest tylko i wyłącznie osobistą opinią autora. Zgodnie z powyższym autor nie ponoszą jakiejkolwiek odpowiedzialności za decyzje inwestycyjne podejmowane na jej podstawie.__

Projekt przygotowany w ramach zadania wykonywane podczas zajęć. Prezentuje przykładową procedurę dopasowania modelu z rodziny ARIMA i prognozowania opartego na tym modelu. Autor nie jest powiązany ze spółką KRUK SA, a wybór danych był motywowany ich niską wariancją.

Kod źródłowy i dokumenty dostępne na prawach licencji MIT.