install.packages("forecast")
library(forecast)

instrumentCena <- read.table(file = "KRUK.txt", sep = ",", header = TRUE)
instrumentCena <- instrumentCena[, 6]

arimaCena <- auto.arima(instrumentCena)
arimaCena

model <- Arima(instrumentCena, order = c(1,1,2), include.drift = TRUE)
prognoza <- forecast(model, h = 50)
plot (prognoza, main ="KRUK SA", xlab = "Dzien notowan", ylab = "Cena")